﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringidFailid
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Kuhi ja järjekord
            //Console.WriteLine("nii toimib järjekord:\n");

            //Queue<int> järjekord = new Queue<int>();
            //järjekord.Enqueue(7);
            //järjekord.Enqueue(2);
            //järjekord.Enqueue(5);
            //Console.WriteLine("Järjekorras on:");
            //foreach (var x in järjekord) Console.Write($"\t{x}");
            //Console.WriteLine();


            //while (järjekord.Count > 0)
            //{
            //    Console.WriteLine(järjekord.Dequeue());
            //}

            //Console.WriteLine("\nnii toimib kuhi:\n");

            //Stack<int> kuhi = new Stack<int>();
            //kuhi.Push(7);
            //kuhi.Push(2);
            //kuhi.Push(5);
            //Console.WriteLine("Kuhjas on:");
            //foreach (var x in kuhi) Console.Write($"\t{x}");
            //Console.WriteLine();

            //while (kuhi.Count > 0)
            //{
            //    Console.WriteLine(kuhi.Pop());
            //} 
            #endregion

            Console.Write("Anna oma nimi: ");
            string nimi = Console.ReadLine();

            //Console.WriteLine(nimi.Substring(5,2));
            //Console.WriteLine(nimi.Substring(5));

            //Console.WriteLine(nimi.ToUpper());
            //Console.WriteLine(nimi.ToLower());
            string[] osad = nimi.Split(' '); // <- siin on char eraldaja

            // TODO: siin vahel tuleks nüüd miskit teha
            // nimi on meil tükeldatud osadeks
            // iga osa algustäht tuleb suureks (ja ülejäänud väikseks) teha
            // seega tuleks massiv läbi käia

            for (int i = 0; i < osad.Length; i++)
            // siin kahjuks ei saa (vist) foreachi kasutada aga võime proovida
            {
                string osa = osad[i]; // see on nüüd üks osa nimest
                if (osa.Length > 0)
                {
                    string[] oskesed = osa.Split('-');
                    for (int j = 0; j < oskesed.Length; j++)
                    {
                        string oskene = oskesed[j];
                        oskesed[j] = oskene.Substring(0, 1).ToUpper() + oskene.Substring(1).ToLower();
                    }
                    osad[i] = string.Join("-", oskesed);
                }
            }
            nimi = string.Join(" ", osad); // <- siin on string eraldaja
            Console.WriteLine(nimi);

            // nii saab stringi tagurpidi pöörata
            //nimi = new string(nimi.Reverse().ToArray());
            //Console.WriteLine(nimi);
        }
    }
}
