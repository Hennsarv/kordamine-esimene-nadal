﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FailidLugemine
{
    class Program
    {
        static void Main(string[] args)
        {
            string failiNimi = @"..\..\Data\test.txt";

            /*
             * PRojekti kaustas @"..\..\failinimi"
             * PRojektikaustas alamkaust (Data) @"..\..\Data\failinimi"
             * Solutioni kaustas @"..\..\..\failinimi
             * Solution alamkasutas @"..\..\..\SolutionData\failinimi"
             * 
             * Mujal @"c:\kaustanimi\failini"
             */

            string[] loetudRead = File.ReadAllLines(failiNimi);
            foreach(var rida in loetudRead)
                Console.WriteLine(rida);
        }
    }
}
