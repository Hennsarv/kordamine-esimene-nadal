﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kokkuvõte1
{
    class Program
    {
        static void Main(string[] args)
        {
            string vastus;
            List<int> vanused = new List<int>();
            List<DateTime> sünnipäevad = new List<DateTime>();
            do
            {

                Console.WriteLine("anna sünnipäev");
                vastus = Console.ReadLine();
                if (DateTime.TryParse(vastus, out DateTime kuupäev))
                {
                    int vanus = DateTime.Now.Year - kuupäev.Year;
                    // TEHA: mõtle ja kontrolli, kas see on ikka vanus
                   
                    DateTime sünnipäev = kuupäev.AddYears(vanus);
                    if (sünnipäev > DateTime.Now) vanus--;
                    Console.WriteLine($"vanus on  {vanus}");
                    vanused.Add(vanus);
                    sünnipäevad.Add(kuupäev);
                }
                else if (vastus != "")
                {
                    Console.WriteLine("kuupäeva ei oska öelda vä...");
                }
            } while (vastus != "");

            // trükime välja sünnipäevad
            foreach (var d in sünnipäevad) Console.WriteLine(d.ToString("dd.MMMM.yyyy"));
            // TODO: nüüd tuleks leida keskmine vanus - kuidas seda teha
            int keskmine = 0;
            foreach (var v in vanused) keskmine += v;
            Console.WriteLine($"keskmine vanus on {keskmine / vanused.Count}");

            Console.WriteLine(vanused.Average());

            //int mitu = 0;
            //int summa = 0;
            //foreach(var x in vanused)
            //{
            //    summa += x; mitu++;
            //}
            //double average = (double)summa / mitu;
            




        }
    }
}
